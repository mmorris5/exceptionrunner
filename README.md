Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

The file logging is set to handle all levels, while the console logging is set to handle up to INFO.

1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

The file logging logs an event of the FINER logging level.

1.  What does Assertions.assertThrows do?

It is ensuring that a piece of code throws a given exception.

1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)

	It is a unique identifier for a serializable class, which allows the class to be converted to a byte stream.

    2.  Why do we need to override constructors?

	This allows us to use our own error message to display, instead of the superclass's.

    3.  Why we did not override other Exception methods?

	The superclass takes care of the rest of the methods.
	
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

It is a block of code that runs when a class is loaded into memory.

1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

README.md is a readme file that is written in Markdown language, which is used by bitbucket.

1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

A variable that is necessary for the finally block is never initialized if the TimerException is thrown.

1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

Because timeToWait is negative, the if statement is true and the exception gets thrown. Once this happens, the finally block is then executed without continuing the rest of the timeMe() function.

1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException

TimerException is Checked, NullPointer Exception is UnChecked.

1.  Push the updated/fixed source code to your own repository.